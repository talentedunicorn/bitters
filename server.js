const express = require('express')
const path = require('path')

const app = express()
const PORT = 3000

const logger = function(req, res, next) {
  // Log requests
  console.log(Date.now().toString(), "[" + req.method + "]:", req.url)
  next()
}

// Middleware
app.use(logger)
app.use(express.static(path.join(__dirname, 'dist')))

// Routes
app.get('/api', (req, res) => {
  res.send('Bitters API')
})

app.get('/api/bitters', (req, res) => {
  let bitters = [
    { name: 'Angostura', origin: 'Venezuela' },
    { name: 'The bitter truth', origin: 'Germany' },
    { name: 'Bittermens', origin: 'U.S.A' },
    { name: 'Peychauds', origin: 'U.S.A' }
  ]
  res.json(bitters)
})

// app.get('*', (req, res) => {
//   res.send(res.data)
// })

app.listen(PORT, () => console.log(`Bitter is running on localhost:${PORT}`))
