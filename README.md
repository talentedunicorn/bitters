# Bitters
> Selection of bitters for cocktails

## Installing

```
$ npm install
```

## Building

```
$ npm run build
```

## Running production

```
$ npm run start
```

## Running development server

```
$ npm run dev
```
